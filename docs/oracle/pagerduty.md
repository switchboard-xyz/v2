---
sidebar_position: 45
---

# Pagerduty

Pagerduty allows you to get real time alerts on your kubernetes cluster. You will need to sign up for an account and get an API key for access.

- [Pagerduty - Generating API Keys](https://support.pagerduty.com/docs/generating-api-keys#section-events-api-keys)

:::note

<b>OPTIONAL</b> $PAGERDUTY_KEY is an optional environment variable to help you manage your cluster. You may wish to ignore this variable if you are comfortable with your own monitoring solutions

:::
