---
sidebar_position: 40
---

import MarkdownImage from "../../src/components/MarkdownImage";

# Google Cloud Platform

You will need a Google Cloud Platform account with the [Cloud SDK installed](https://cloud.google.com/sdk/docs/install).

## Login

Once installed, login to your google cloud account:

```bash
gcloud auth login
```

## Project

Create a new project

```bash
gcloud projects create switchboard-oracle-cluster --name="Switchboard Oracle"
```

Set it as your default project

```bash
gcloud config set project switchboard-oracle-cluster
```

[Google - gcloud projects create](https://cloud.google.com/sdk/gcloud/reference/projects/create)

## gcloud Config

Set the default zone using [list of regions and zones](https://cloud.google.com/compute/docs/regions-zones#available)

```bash
gcloud config set compute/zone us-west1-a # replace with your closest region
```

Set the default region using [list of regions and zones](https://cloud.google.com/compute/docs/regions-zones#available)

```bash
gcloud config set compute/region us-west1 # replace with your closest region
```

[Google - Set default settings for the gcloud tool](https://cloud.google.com/kubernetes-engine/docs/quickstart#autopilot)

## Billing

You will need to enable billing for the project before enabling any services:

- https://console.cloud.google.com/billing/enable?project=switchboard-oracle-cluster

[Google - APIs and billing](https://support.google.com/googleapi/answer/6158867?hl=en)

## Services

Enable the relevant services:

```bash
gcloud services enable container.googleapis.com
gcloud services enable iamcredentials.googleapis.com
gcloud services enable secretmanager.googleapis.com
```

## Authentication

You will need to create the credentials to make changes to your google account.

First you will need to [enable OAuth consent](https://console.cloud.google.com/apis/credentials/consent) for our application. [(...more information)](https://support.google.com/cloud/answer/6158849?authuser=3#userconsent&zippy=%2Cuser-consent)

Next you need to create the credentials by navigating to [APIs & Services > Credentials](https://console.cloud.google.com/apis/credentials), click **+ Create Credentials**, then select **OAuth Client Id** from the drop down

> ![GCP Credentials Dashboard](/img/gcp/Credentials_Dashboard.png)

Set the Application type to `Web application` and give it a name, then select **Create**

> ![GCP Credentials Create OAuth](/img/gcp/Create_OAuth.png)

Note your client ID and secret for the ENV file and download the JSON for safekeeping. This will be your `$GOOGLE_AUTH_CLIENT_ID` and `$GOOGLE_AUTH_CLIENT_SECRET`

> <MarkdownImage
>   img="/img/gcp/OAuth_Client_Credentials.png"
>   unstyled
>   sx={{ width: "80%" }}
> />

[Google - Setting up OAuth 2.0](https://support.google.com/cloud/answer/6158849?authuser=3)

## Static IP

You will need to reserve a static IP address for your grafana instance

```bash
gcloud compute addresses create load-balancer --project=switchboard-oracle-cluster
gcloud compute addresses list
# NAME           ADDRESS/RANGE
# load-balancer  123.123.123.123 ($LOADBALANCER_IP)
```

<!-- Could we use a grep to grab this value easier? -->

This will be your `$LOADBALANCER_IP`

[Google - Reserve a new static external IP address](https://cloud.google.com/compute/docs/ip-addresses/reserve-static-external-ip-address#reserve_new_static)

## Service Account

You will need to create a service account to access our resources.

```bash
gcloud iam service-accounts create svc-account --display-name="Oracle Service Account"
gcloud iam service-accounts list
```

Now save it to our filesystem

```bash
gcloud iam service-accounts keys create secrets/svc-account.json  --iam-account=svc-account@switchboard-oracle-cluster.iam.gserviceaccount.com
```

Now convert the json file to a base64 string to store in `$SERVICE_ACCOUNT_BASE64`

```bash
base64 secrets/svc-account.json
```

[Google - Creating service account keys](https://cloud.google.com/iam/docs/creating-managing-service-account-keys)

## Google Secret Manager

You will need to store your solana keypair in Google Secret Manager for enhanced security. If you are using another keypair replace `--data-file` with your relevant path.

```bash
gcloud secrets create oracle-payer-secret --replication-policy="automatic"  --data-file=secrets/authority-keypair.json
```

You can view your `$GOOGLE_PAYER_SECRET_PATH` in the [GCP console](https://console.cloud.google.com/security/secret-manager/secret/oracle-payer-secret/versions&project=switchboard-oracle-cluster) or by running the command

```bash
echo "$(gcloud secrets list --uri --filter=oracle-payer-secret | \
cut -c41- | tr -d '\n')/versions/latest"
```

[Google - Creating a secret](https://cloud.google.com/secret-manager/docs/creating-and-accessing-secrets)

## Kubernetes Cluster

Finally you will need to create a new kubernetes cluster

```bash
gcloud container clusters create-auto switchboard-cluster \
--service-account=svc-account@switchboard-oracle-cluster.iam.gserviceaccount.com \
--region us-west1
```

then connect to it and store your credentials in your gCloud config

```bash
gcloud container clusters get-credentials switchboard-cluster \
--project switchboard-oracle-cluster \
--region us-west1
```

:::note
Remember to update the region to the same region you used for your static IP.
:::

[Google - Create a GKE cluster](https://cloud.google.com/kubernetes-engine/docs/quickstart#autopilot)
